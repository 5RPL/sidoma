//LOGIN, lOGOUT, Register System
//error0=>email tidak terdaftar
//error1=>password email salah
//error2=>internal error
//true=>rubah menu

//login
const loginBtn=document.getElementById("loginbtn");
const daftarBtn=document.getElementById("daftarbtn");
const AccountBtn=document.getElementById("akunMenu");
login= document.getElementById("login");
$(login).submit(function (e) {
    e.preventDefault();
    const LoginToSystem=(user,password)=>
    {
        let toReturn=AjaxToServer();

        function AjaxToServer() {
            //ajaxToserver expected error0,error1,error2
            //Error2 check connection to server
            let ajaxcheckConnection=true;
            let ajaxLogin=true;
            if(ajaxcheckConnection===true)
            {
                if(ajaxLogin===true){
                    return true;
                }
                else
                {
                    return ajaxLogin;
                }
            }
            else
            {
                return "error2";
            }

        }
        return toReturn;
    }

    let check=LoginToSystem("user","psw");
    const loginInfo=document.getElementById("errorLogin");
    switch (check) {
        case "error2":
        {
            loginInfo.innerText="Internal error";
            break;
        }
        case "error1":
        {
            loginInfo.innerText="Password salah";
            break;
        }
        case "error0":
        {
            loginInfo.innerText="Email/Username tidak terdaftar";
            break;
        }
        case true :
        {
            loginBtn.classList.add("toHide");
            daftarBtn.classList.add("toHide");
            AccountBtn.classList.remove("toHide");
            $("#form-login").modal("hide");
        }
    }
});
//logout
//error3=>anda belum login (keamanan sistem) javascript dapat di manipulasi dari client side
//error4=>kesalahan server atau client sedang offline
$("#logoutBTN").on("click",function () {

    document.getElementById("infoText").innerHTML="Apakah anda yakin untuk keluar?";
    document.getElementById("konfirmasiYA").classList.remove("toHide");
    document.getElementById("konfirmasiTidak").classList.remove("toHide");
    $("#infoModal").modal("show");

    $("#konfirmasiYA").on("click",function () {
        let check=ajaxToSystem();


        switch (check) {
            case true:
            {
                loginBtn.classList.remove("toHide");
                daftarBtn.classList.remove("toHide");
                AccountBtn.classList.add("toHide");
                document.getElementById("konfirmasiYA").classList.add("toHide");
                document.getElementById("konfirmasiTidak").classList.add("toHide");
                let timer=3;
                function countdown()
                {
                    if (timer => 0) {
                        document.getElementById("infoText").innerHTML="Anda telah logout, halaman akan di restart otomatis dalam "+timer+" detik";
                        timer--;
                        setTimeout(countdown, 1000);
                    }
                }
                countdown();
                $("#infoModal").modal("show");
                setTimeout(function () {
                    $("#infoModal").modal("hide");
                    location.href="index.html";
                    timer=3;
                }, 4000);

                break;
            }
            case "error3":
            {

                document.getElementById("infoText").innerHTML="anda belum login ";
                document.getElementById("konfirmasiYA").classList.add("toHide");
                document.getElementById("konfirmasiTidak").classList.add("toHide");
                $("#infoModal").modal("show");
                setTimeout(function () {
                    location.href="index.html";
                }, 3000);
                break;
            }
            case "error4":
            {
                loginBtn.classList.remove("toHide");
                daftarBtn.classList.remove("toHide");
                AccountBtn.classList.add("toHide");
                document.getElementById("konfirmasiYA").classList.add("toHide");
                document.getElementById("konfirmasiTidak").classList.add("toHide");
                let timer=3;
                function countdown()
                {
                    if (timer => 0)
                    {
                        document.getElementById("infoText").innerHTML="kesalahan server atau anda sedang offline, pesan ini akun otomatis di tutup dalam "+timer+" detik";
                        timer--;
                        setTimeout(countdown, 1000);
                    }
                }
                countdown();
                $("#infoModal").modal("show");
                setTimeout(function () {
                    $("#infoModal").modal("hide");
                    timer=3;
                }, 4000);
                break;
            }
        }
    });
    $("#konfirmasiTidak").on("click",function () {
        $("#infoModal").modal("hide");
    });
    function ajaxToSystem() {
        let checkConnection=true;
        let cekLogout=true;

        if(checkConnection===true)
        {
            if(cekLogout===true)
            {
                return true;
            }
            else
            {
                return cekLogout;
            }
        }
        else
        {
            return "tidak dapat terhubung ke server atau anda sedang offline";
        }
    }
});
const Element_Generator=(elTag=null,elClass=[null],elAttrib=[null],elText=null,elchild=[null])=>
{
    let toReturn=null
    if(elTag!==null)
    {
        toReturn= document.createElement(elTag);
        if(elClass[0]!==null)
        {
            for(let i =0;i<elClass.length;i++)
            {
                toReturn.classList.add(elClass[i]);
            }
        }
    }


    if(elAttrib[0]!==null)
    {
        for(let i =0 ;i<elAttrib.length;i++)
        {
            addAttribute(elAttrib[i]);
        }
    }

    function addAttribute(attribute) {
        attribute=attribute.split(":");
        toReturn.setAttribute(attribute[0], attribute[1]);
    }

    if(elText!==null)
    {
        toReturn.innerHTML=elText;
    }


    if(elchild[0]!==null)
    {
        for (let i=0; i<elchild.length;i++)
        {
            let to_Append=Element_Generator(elchild[i][0],elchild[i][1],elchild[i][2],elchild[i][3],elchild[i][4]);
            toReturn.appendChild(to_Append);
        }
    }

    return toReturn;
}
